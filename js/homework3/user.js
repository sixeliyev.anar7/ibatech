function createNewUser() {
    var name = prompt("enter name");
    var surname = prompt("enter surname");
    var newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function () {
            var res = this.firstName.charAt(0);
            res = res.toLocaleLowerCase();
            var sur = this.lastName.toLowerCase();
            return res + sur;
        },
        setFirstName: function () {
            this.firstName = prompt("change name into");
        },
        setLastName: function () {
            this.lastName = prompt("change lastname into");
        }
    }
    if (confirm("do you want to change name/surname")) {
        newUser.setFirstName();
        newUser.setLastName();
    }
    console.log(newUser.getLogin());
}

createNewUser();