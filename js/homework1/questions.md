1)declaring variables via var,let and const:
    First i want to mention deifference between 'let' and 'var'. As i understood when we declare variables with var, they can be re-declared(declare again using 'var') and updated(changing its content). However with 'let', variables can be updated but we can not declare it again(no re-declaration).
    'const' is almost exactly the same as 'let' , but when you decalare variable with 'const' you cannot change its value and it is also block scoped same as 'let'. 

2) problem with 'var':
    when you declare variable with var and then change its value in some func,if/else,loop, sometimes this can create some problem if you have used this variable in some other part of your code and it can cause some bugs in your code. So it is more comfartable to use let instead.