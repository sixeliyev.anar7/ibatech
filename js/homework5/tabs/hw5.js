function Switch(switchtab_id, switchtab_content) {
    var x = document.getElementsByClassName("tabs-content");
    for (var i = 0; i < x.length; i++) {
        x[i].style.display = 'none'; // hide all tab content
    }
    document.getElementById(switchtab_content).style.display = 'block';
    var y = document.getElementsByClassName("tabs");
    for (var i = 0; i < y.length; i++) {
        y[i].className = 'tabs';
    }
    document.getElementById(switchtab_id).className = 'tabs active';
}